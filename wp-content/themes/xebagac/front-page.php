<?php get_header(); ?>

<?php
	$banner_image = get_field('banner_image', 'option');
	$banner_title = get_field('banner_title', 'option');
	$banner_desc = get_field('banner_desc', 'option');

	$home_strong_title = get_field('home_strong_title', 'option');
	$home_strong_content = get_field('home_strong_content', 'option');

	$home_place = get_field('home_place', 'option');

	$home_service_title = get_field('home_service_title', 'option');
	$home_service_content = get_field('home_service_content', 'option');

	$home_price_title = get_field('home_price_title', 'option');
	$home_table_price = get_field('home_table_price', 'option');

	// show data book car
	global $wpdb;
	$sql = "SELECT * FROM {$wpdb->prefix}cf7_vdata_entry";
    $results = $wpdb->get_results($sql);

    $datalist = [];
    foreach ($results as $key => $value) {
    	$datalist[$value->data_id][] = $value;
    }
?>

<style>
	.price {
		background: url('<?php echo get_option('home');?>/wp-content/themes/xebagac/dist/images/bgprice.jpg') no-repeat center center;
		background-size: cover;
		background-attachment: fixed;
	}
</style>


<main class="index">

<section class="slider">
	<div class="slider-banner">
		<img src="<?php echo $banner_image; ?>" title="" alt="" class="w-100">
	</div>
	<div class="slider-container">
		<div class="container">
			<div id="contact" class="slider-content">
				<div class="slider-content-wrap">
					<h1 class="s60 bold"><?php echo $banner_title; ?></h1>
					<h2 class="bold s24 pt-3 pb-4"><?php echo $banner_desc; ?></h2>
				</div>

				<div class="index-frm">
					<div class="row">
						<div class="col-lg-5">
							<?php get_template_part("resources/views/form-book-car"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-lg-7">
				<div class="index-frm-r" style="position: relative; z-index: 1;">
					<h2 class="s24 bold t2 text-lg-left text-center cus-tit">
						Những khách hàng đã đặt xe của chúng tôi
					</h2>

					<div class="owl-carousel cus-slider">
					<!-- <div class="cus-slider"> -->

					    <?php
					    	$i = 1;
					    	foreach ($datalist as $key => $datalist_value) {
					    		$data_date = $datalist_value[6]->value;
					    		$data_title = $datalist_value[4]->value;
						?>

							<div class="bg-white d-flex align-items-start cus-item">
								<img src="<?php echo asset('images/icon1.png'); ?>" title="" alt="">
								<div class="t2 s13 cus-item-info">
									<h3 class="bold s16 text-capitalize cus-item-info-name">
										<?php echo $data_title; ?>
									</h3>
									<p><strong>Đã đặt lúc:</strong> 
										<?php echo $data_date; ?>
									</p>
								</div>
							</div>

						<?php $i++; } ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="b4 tm">
	<div class="container">
		<div class="row">

			<div class="col-lg-3">
				<h2 class="strengths__title font-weight-bold thin s72 t2 text-lg-left text-center tm-tit wow fadeInLeft">
					<?php echo $home_strong_title; ?>
				</h2>
			</div>

			<div class="col-lg-8 offset-lg-1">
				<div class="row justify-content-between">

	                <?php
	                    foreach ($home_strong_content as $home_strong_content_kq) {

	                    $post_title = $home_strong_content_kq['title'];
	                    $post_image = $home_strong_content_kq['image'];
	                    $post_desc = $home_strong_content_kq['desc'];
	                ?>

					<div class="col-lg-5 col-sm-6 d-flex justify-content-center wow fadeInUp"
						data-wow-delay=".2s">
						<div class="text-center tm-item">
							<img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
							<h3 class="bold s18 py-3">
								<?php echo $post_title; ?>
							</h3>
							<div class="tm-item-wrap">
								<p><?php echo $post_desc; ?></p>
							</div>
						</div>
					</div>

					<?php } ?>

				</div>
			</div>

		</div>
	</div>
</section>


<section class="pj">
	<div class="container-flush">
		<div class="owl-carousel project-wrap">

            <?php
                foreach ($home_place as $home_place_kq) {

                $post_title = $home_place_kq['title'];
                $post_image = $home_place_kq['image'];
            ?>

				<article class="pj-item">
					<figure class="text-center pj-item-img">
						<a href="javascript:void(0)" title="<?php echo $post_title; ?>">
							<img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
						</a>
					</figure>
					<figcaption class="text-center pj-content">
						<h2 class="s30 medium text-uppercase text-white shadow">
							<?php echo $post_title; ?>
						</h2>
					</figcaption>
				</article>

			<?php } ?>

		</div>
	</div>
</section>


<section class="car">
	<div class="container">
		<div class="d-flex align-items-center justify-content-between flex-wrap align-items-center">
			<h2 class="title__service s24 text-md-left text-center text-uppercase">
				<?php echo $home_service_title; ?>
			</h2>
		</div>
		<div class="owl-carousel car-slider">

            <?php
                foreach ($home_service_content as $home_service_content_kq) {

				$post_id = $home_service_content_kq->ID;
        		$post_title = get_the_title($post_id);
                $post_content = get_the_content($post_id);
        		$post_date = get_the_date('Y/m/d', $post_id);
        		$post_link = get_post_permalink($post_id);
        		$post_image = getPostImage($post_id,"p-service");
        		$post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
            ?>

				<article class="car-item">
					<a href="<?php echo $post_link; ?>">
						<figure class="text-center car-item-img">
							<img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
						</figure>
						<figcaption class="car-item-info">
							<h3 class="service__title bold s14 text-uppercase pb-3 car-item-info-tit">
								<?php echo $post_title; ?>
							</h3>
							<div class="service__desc">
								<p>
									<?php echo $post_excerpt; ?>
								</p>
							</div>
						</figcaption>
					</a>
				</article>

			<?php } ?>
			
		</div>
	</div>
</section>


<section class="text-white price">
	<div class="container">
		<h2 class="s36 medium text-center text-uppercase price-tit">
			<?php echo $home_price_title; ?>
		</h2>

		<div class="row justify-content-center">
			<div class="col-lg-12">
				<div class="bg-white price-tbl">
					<div class="table-responsive">

						<table class="table table-header cusTable">
							<thead class="text-uppercase medium s13">
								<tr>
									<th scope="col">Đoạn đường</th>

					                <?php $i = 1;
					                    foreach ($home_table_price as $home_table_price_kq) {
											if($i == 1) {
												foreach ($home_table_price_kq['way'] as $home_table_way_kq) {
													?>
														<th scope="col">
															<?php echo $home_table_way_kq["way_service"][0]->post_title; ?>
														</th>
													<?php
												}
											}
					                		$i++;
					                	}
					                ?>

								</tr>
							</thead>
							<tbody class="s13 text-uppercase t4">
								
				                <?php
				                    foreach ($home_table_price as $home_table_price_kq) {
								?>
								<tr>
									<th scope="row"><i class="far fa-dot-circle"></i> 
										<?php echo $home_table_price_kq["way_title"]; ?>
									</th>
			                    		

		                    		<?php
		                    			foreach ($home_table_price_kq["way"] as $home_table_price_way_kq) {
                    				?>
                    					<td><?php echo $home_table_price_way_kq["way_service_price"]; ?></td>
                    				<?php } ?>
                				</tr>
								<?php } ?>
								
							</tbody>

						</table>

					</div>
				</div>
			</div>
		</div>

	</div>
</section>


</main>

<?php get_footer(); ?>

