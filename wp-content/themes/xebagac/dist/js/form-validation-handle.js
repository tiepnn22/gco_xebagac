$(function(){
    $.validator.addMethod("valueNotEquals",function(value,element,args){
        return args !== value;
    },'Chọn xe bạn muốn.');

    $("#tel").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#regis-frm").validate({
        rules: {
            vehice: {
                    valueNotEquals:"default",
            },
            start:{
                required:true/*,
                minlength:3*/

            },
            target: {
                required:true,
                minlength:5
            },
            startime: {
                required: true
                /*minlength: 5*/
            },
            endtime: {
                required: true/*,
                minlength: 5*/
            },
            quan: {
                required: true,
                /*minlength: 5,
                equalTo: "#password"*/
            },
            name: {
                required: true/*,
                email: true*/
            },
            tel: "required"
        },

        //noti
        messages: {
            vehice: {
                valueNotEquals:"Chọn xe bạn muốn.",
            },
            /*name: "Nhập họ tên của bạn",*/
            start:{
                required:'Nhập địa điểm đón bạn.',
                minlength:'chưa dủ 3 ký tự'
            },

            target: {
                required: "Nhập địa điểm bạn sẽ tới."
                /*minlength:'Số điện thoại không phù hợp',
                maxlength:'Số điện thoại không phù hợp',*/
            },
            startime: {
                required: "Nhập thời gian chúng tôi sẽ đón bạn."
                /*minlength: "Tài khoản đăng nhập phải có ít nhất {0} ký tự"*/
            },
            quan: {
                required: "Nhập số người đi xe."
                /*minlength: "Mật khẩu phải có ít nhất {0} ký tự"*/
            },
            name: {
                required: "Nhập họ tên của bạn.",
                minlength: "Mật khẩu phải có ít nhất {0} ký tự"
                /*equalTo: "Nhập lại mật khẩu chưa đúng"*/
            },
            tel: "Nhập số điện thoại của bạn.",
            endtime: {
                required: "Nhập thời gian bạn sẽ về."
            }
        }
    });
})