$(document).ready(function($) {

    // Ajax Readmore_post Category
    var data_offset = 7;
    $("body.category .smore-btn").click(function(e) {
        var data_catid = $(this).data('catid');

        jQuery.ajax({
            type: "POST",
            url: ajax_url,
            data: {
                action: 'Readmore_post',
                data_offset : data_offset,
                data_catid : data_catid
            },
            success:function(response){
                data_offset = data_offset + 7;
                console.log('offset'+data_offset);

                data = jQuery.parseJSON(response);
                $(".bpage-link").before(data.result);
                if(data.exit == 2) {
                    alert('Nội dung đang được cập nhật.');
                }
            }
        });
    });


    // Ajax Readmore_post Search
    var data_offset_search = 7;
    $("body.search .smore-btn").click(function(e) {
        var data_keyword = $(this).data('catid');
        console.log(data_keyword);

        jQuery.ajax({
            type: "POST",
            url: ajax_url,
            data: {
                action: 'Readmore_post_search',
                data_offset_search : data_offset_search,
                data_keyword : data_keyword
            },
            success:function(response){
                data_offset_search = data_offset_search + 7;
                console.log('offset'+data_offset_search);

                data = jQuery.parseJSON(response);
                $(".bpage-link").before(data.result);
                if(data.exit == 2) {
                    alert('Nội dung đang được cập nhật.');
                }
            }
        });
    });



    if ($('header.top').length) {
        $(window).scroll(function() {
            var anchor = $('header.top').offset().top;
            if (anchor >= 130) {
                $('header.top').addClass('cmenu');
                $('.tmenu-cate').slideUp('fast');
            } else {
                $('header.top').removeClass('cmenu');
            }
        });
    }

    $('.tcate-tit').on('click', function(event) {
        event.preventDefault();
        $(this).next('.tmenu-cate').slideToggle('fast');
    });
    $('.popup-close').on('click', function(event) {
        event.preventDefault();
        $('.top-popup').slideUp();
    });

    new WOW().init();

    if ($('.backTop').length) {
        $('.backTop').on('click', function(event) {
            event.preventDefault();
            $('body, html').stop().animate({ scrollTop: 0 }, 800)
        });
        $(window).scroll(function() {
            var anchor = $('.backTop').offset().top;
            if (anchor > 1000) {
                $('.backTop').css('opacity', '1')
            } else {
                $('.backTop').css('opacity', '0')
            }
        });
    }

    $("#menu").mmenu({
        "extensions": [
            "pagedim-black",
            "shadow-panels"
        ]
    }, {
        clone: true
    });

    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $('.owl-carousel.car-slider').owlCarousel({
        items: 4,
        lazyLoad: true,
        nav: true,
        margin: 20,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 3000,
        smartSpeed: 400,
        rewind: true,
        navText: ['', ''],
        // navText: ['<img src="../../images/left.png" alt=""></img>', '<img src="../images/right.png" alt=""></img>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });

    $('.owl-carousel.project-wrap').owlCarousel({
        items: 4,
        lazyLoad: true,
        margin: 0,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4000,
        smartSpeed: 400,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 4
            }
        }
    });

    $('.owl-carousel.blog-slider').owlCarousel({
        items: 3,
        margin: 50,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4000,
        smartSpeed: 4000,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    $('.owl-carousel.bdetail-re-slider').owlCarousel({
        items: 3,
        margin: 30,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4000,
        smartSpeed: 400,
        rewind: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });

    $('.owl-carousel.cus-slider').owlCarousel({
        items: 2,
        margin: 18,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4000,
        smartSpeed: 4000,
        slideTransition: 'linear',
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2
            }
        }
    });



    if ($("[data-fancybox]").length) { $("[data-fancybox]").fancybox({}) }
    if ($('.linkyoutube').length) {
        var url = $('.linkyoutube').attr('href').replace('watch?v=', 'embed/');
        $('.linkyoutube').attr('href', url);
    }
    var w = $('.pj-item-img img').width();

    $('.pj-item-img img').css('height', w);
    $(window).resize(function(event) {
        var w = $('.pj-item-img img').width();

        $('.pj-item-img img').css('height', w);
    });

    $('.swicth').on('click', function(event) {
        event.preventDefault();
        $('.swicth-wrap').toggleClass('on');
    });

    // $('.datetime').datetimepicker({
    //     icons: {
    //         time: "far fa-clock",
    //         date: "fas fa-calendar-alt",
    //         up: "fas fa-angle-up",
    //         down: "fas fa-angle-down"
    //     }
    // });
});