<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<script>
	var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>

<header class="fixed-top top">
	<div class="container">
		<div class="w-100 d-flex align-items-center justify-content-between top-menu-control">

			<div class="d-flex align-items-center justify-content-between top-menu-btn">
				<a href="<?php echo get_option('home');?>" title="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>" class="text__logo">
					<img src="<?php echo get_field('h_logo', 'option'); ?>" alt="<?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?>" class="logo">
					<span>
						<?php echo get_field('h_logo_name', 'option'); ?>
					</span>
				</a>
			</div>

			<div class="d-flex align-items-center menu-r">

				<div class="menu-r-call">
					<a href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>" title="" class="s16 bold d-flex align-items-center">
						<img src="<?php echo asset('images/phone.png'); ?>" title="" alt="">
						<span class="d-md-inline-block d-none"><?php echo get_field('h_phone', 'option'); ?></span>
					</a>
				</div>

				<a id="nav-icon" href="#menu" class="d-xl-none ml-2">
					<span></span>
					<span></span>
					<span></span>
				</a>
				<div class="d-flex align-items-center justify-content-between">
					<nav id="menu" class="">
				        <?php
				            if(function_exists('wp_nav_menu')){
				                $args = array(
				                    'theme_location' => 'primary',
				                    'container_class'=>'menu-menu-chinh-container',
				                    'menu_class'=>'menu'
				                );
				                wp_nav_menu( $args );
				            }
				        ?>
				    </nav>
				</div>
				
			</div>

		</div>
	</div>
</header>
