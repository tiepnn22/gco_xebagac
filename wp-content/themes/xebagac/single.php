<?php get_header(); ?>

<?php
	$get_category =  get_the_category(get_the_ID());
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
?>

<main class="index">

<section class="blog">
	<div class="container">
		<div class="d-flex align-items-center justify-content-between flex-wrap bpage-header">
			<h2 class="s48 light bpage-tit">
				<?php echo $cat_name; ?>
			</h2>

			<?php get_template_part("resources/views/form-search"); ?>
		</div>

		<div class="bdetail-wrap">
			<div class="row">

				<div class="col-lg-9 c0l-md-8">

					<div class="bcontent">
						
						<div class="text-center">
							<img src="<?php echo getPostImage( get_the_ID() ,"full"); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
						</div>

						<time datetime="2018-06-04" class="t2 s14 py-3 btime">
							<?php echo customDate( get_the_ID() ); ?>
						</time>
						
						<h1 class="light s36 bdetail-tit">
							<?php the_title(); ?>
						</h1>

						<p>
							<?php echo wpautop( the_content() ); ?>
						</p>

					</div>

					<?php get_template_part("resources/views/social-bar"); ?>

					<?php get_template_part("resources/views/template-related-post"); ?>

				</div>
				
				<?php get_sidebar();?>

			</div>
		</div>

	</div>
</section>

</main>

<?php get_footer(); ?>