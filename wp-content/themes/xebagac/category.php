<?php get_header(); ?>

<?php
	$category_info = get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id = $category_info->term_id;
	$cat_name = get_cat_name($cat_id);
	$cat_link = esc_url(get_term_link($cat_id));

	$home_service_content = get_field('home_service_content', 'option');
?>

<main class="index">

<section class="blog bpage">
	<div class="container">

		<div class="d-flex align-items-center justify-content-between flex-wrap bpage-header">
			<h1 class="s48 light bpage-tit">
				<?php echo $cat_name; ?>
			</h1>

            <?php get_template_part("resources/views/form-search"); ?>
		</div>

		<div class="owl-carousel blog-slider">

            <?php
                foreach ($home_service_content as $home_service_content_kq) {

				$post_id = $home_service_content_kq->ID;
        		$post_title = get_the_title($post_id);
                $post_content = get_the_content($post_id);
        		$post_date = customDate($post_id);
        		$post_link = get_post_permalink($post_id);
        		$post_image = getPostImage($post_id,"p-service");
        		$post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
            ?>

				<article class="bg-white bslider-item">
					<figure class="text-center bslider-img">
						<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
							<img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
						</a>
					</figure>
					<figcaption class="bslider-content">
						<time datetime="2018-06-04" class="t2 s14 btime">
							<?php echo $post_date; ?>
						</time>
						<h3 class="s24 light bslider-content-tit">
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<?php echo $post_title; ?>
							</a>
						</h3>
						<div class="bslider-content-wrap">
							<p>
								<?php echo $post_excerpt; ?>
							</p>
						</div>
						<div class="text-lg-left text-center">
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="btn more-btn">
								Chi tiết
							</a>
						</div>
					</figcaption>
				</article>

			<?php } ?>

		</div>

		<div class="sblog">
			<div class="row">
				<div class="col-lg-9 col-md-7">

					<?php
						$query = query_post_by_category($cat_id, 7);
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

						$post_id = get_the_ID();
		        		$post_title = get_the_title($post_id);
		                $post_content = get_the_content($post_id);
		        		$post_date = customDate($post_id);
		        		$post_link = get_post_permalink($post_id);
		        		$post_image = getPostImage($post_id,"p-service");
		        		$post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
					?>

						<div class="sblog-item">
							<div class="row">
								<div class="col-lg-5">
									<figure class="text-center sblog-img">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
										</a>
									</figure>
								</div>
								<div class="col-lg-7">
									<figcaption class="bslider-content">
										<time datetime="2018-06-04" class="t2 s14 btime">
											<?php echo $post_date; ?>
										</time>
										<h3 class="s24 light bslider-content-tit">
											<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
												<?php echo $post_title; ?>
											</a>
										</h3>

										<div class="bslider-content-wrap">
											<p>
												<?php echo $post_excerpt; ?>
											</p>
										</div>
										<div class="text-lg-left text-center">
											<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" class="btn more-btn">
												Chi tiết
											</a>
										</div>
									</figcaption>
								</div>
							</div>
						</div>

                    <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

					<div class="text-center pt-5 bpage-link">
						<a href="javascript:void(0)" title="" class="btn smore-btn" data-catid='<?php echo $cat_id; ?>'>Xem thêm</a>
					</div>
				</div>

				<?php get_sidebar();?>

			</div>
		</div>
	</div>
</section>

</main>
	
<?php get_footer(); ?>