<?php
//Register Style
function theme_Style() {
    wp_register_style( 'bootstrap-style', get_stylesheet_directory_uri() . "/dist/css/bootstrap.min.css",false, 'all' );
    wp_enqueue_style('bootstrap-style');
    wp_register_style( 'mmenuall-style', get_stylesheet_directory_uri() . "/dist/css/jquery.mmenu.all.css",false, 'all' );
    wp_enqueue_style('mmenuall-style');
    wp_register_style( 'mmenushadows-style', get_stylesheet_directory_uri() . "/dist/css/jquery.mmenu.shadows.css",false, 'all' );
    wp_enqueue_style('mmenushadows-style');
    wp_register_style( 'owlcarousel-style', get_stylesheet_directory_uri() . "/dist/css/owl.carousel.min.css",false, 'all' );
    wp_enqueue_style('owlcarousel-style');
    wp_register_style( 'owltheme-style', get_stylesheet_directory_uri() . "/dist/css/owl.theme.default.min.css",false, 'all' );
    wp_enqueue_style('owltheme-style');
    wp_register_style( 'animate-style', get_stylesheet_directory_uri() . "/dist/css/animate.css",false, 'all' );
    wp_enqueue_style('animate-style');
    wp_register_style( 'fontawesome-style', get_stylesheet_directory_uri() . "/dist/fonts/fontawesome/all.min.css",false, 'all' );
    wp_enqueue_style('fontawesome-style');
    wp_register_style( 'nivo-style', get_stylesheet_directory_uri() . "/dist/css/nivo-slider.css",false, 'all' );
    wp_enqueue_style('nivo-style');
    wp_register_style( 'formvalidation-style', get_stylesheet_directory_uri() . "/dist/css/form-validation.css",false, 'all' );
    wp_enqueue_style('formvalidation-style');
    // wp_register_style( 'datetimepicker-style', get_stylesheet_directory_uri() . "/dist/css/bootstrap-datetimepicker.css",false, 'all' );
    // wp_enqueue_style('datetimepicker-style');
    wp_register_style( 'style-style', get_stylesheet_directory_uri() . "/dist/css/style.css",false, 'all' );
    wp_enqueue_style('style-style');


    wp_register_script( 'jquery-script', get_stylesheet_directory_uri() . "/dist/js/jquery.min.js", array('jquery'),false,true );
    wp_enqueue_script('jquery-script');
    wp_register_script( 'mmenu-script', get_stylesheet_directory_uri() . "/dist/js/jquery.mmenu.min.all.js", array('jquery'),false,true );
    wp_enqueue_script('mmenu-script');
    wp_register_script( 'carousel-script', get_stylesheet_directory_uri() . "/dist/js/owl.carousel.min.js", array('jquery'),false,true );
    wp_enqueue_script('carousel-script');
    wp_register_script( 'popper-script', get_stylesheet_directory_uri() . "/dist/js/popper.min.js", array('jquery'),false,true );
    wp_enqueue_script('popper-script');
    wp_register_script( 'moment-script', get_stylesheet_directory_uri() . "/dist/js/moment-with-locales.js", array('jquery'),false,true );
    wp_enqueue_script('moment-script');
    wp_register_script( 'bootstrap-script', get_stylesheet_directory_uri() . "/dist/js/bootstrap.min.js", array('jquery'),false,true );
    wp_enqueue_script('bootstrap-script');
    wp_register_script( 'wow-script', get_stylesheet_directory_uri() . "/dist/js/wow.min.js", array('jquery'),false,true );
    wp_enqueue_script('wow-script');
    wp_register_script( 'fontawesome-script', get_stylesheet_directory_uri() . "/dist/fonts/fontawesome/all.min.js", array('jquery'),false,true );
    wp_enqueue_script('fontawesome-script');
    // wp_register_script( 'datetimepicker-script', get_stylesheet_directory_uri() . "/dist/js/bootstrap-datetimepicker.min.js", array('jquery'),false,true );
    // wp_enqueue_script('datetimepicker-script');
    wp_register_script( 'main-script', get_stylesheet_directory_uri() . "/dist/js/main.js", array('jquery'),false,true );
    wp_enqueue_script('main-script');


    // $protocol = 'http://';
    // $params = array(
    //     'ajax_url' => admin_url('admin-ajax.php', $protocol),
    // );
    // wp_localize_script('template-scripts', 'ajax_obj', $params);
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'theme_Style');


// Remove WP Version Css Js
// function remove_ver_css_js( $src ) {
//     if ( strpos( $src, 'ver=' ) )
//         $src = remove_query_arg( 'ver', $src );
//     return $src;
// }
// if (!is_admin()) add_filter( 'style_loader_src', 'remove_ver_css_js', 9999 );
// if (!is_admin()) add_filter( 'script_loader_src', 'remove_ver_css_js', 9999 );


//Register Menu and image
if (!function_exists('theme_Setup')) {
    function theme_Setup()
    {
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'text_domain' ),
        ) );

        // add_theme_support( 'woocommerce' );
	    add_theme_support( 'post-thumbnails' );
	    add_image_size( 'p-service', 360, 250, true );
    }
    add_action('after_setup_theme', 'theme_Setup');
}


//Register Sidebar
if (!function_exists('theme_Widgets')) {
    function theme_Widgets()
    {
        $sidebars = [
            [
				'name'          => __( 'Vùng quảng cáo', 'text_domain' ),
				'id'            => 'adv-ads',
				'description'   => __( 'Vùng quảng cáo dưới chân trang', 'text_domain' ),
				'before_widget' => '<div id="%1$s" class="widget col-lg-6 col-md-6 col-sm-6 col-xs-6 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'theme_Widgets');
}


//Shortcode play on Widget
add_filter('widget_text','do_shortcode');
//Use Block Editor default for Post
// add_filter('use_block_editor_for_post', '__return_false');
//Shortcode play on content input Contact form 7
// add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements' );
// function mycustom_wpcf7_form_elements( $form ) {
//     $form = do_shortcode( $form );
//     return $form;
// }


//Delete class and id of wp_nav_menu()
function wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


//Filter Search
// if( !is_admin() ) {
//     function filter_search($query) {
//         if ($query->is_search) {
//             $query->set('post_type', array('post'));
//         };
//         return $query;
//     };
//     add_filter('pre_get_posts', 'filter_search');
// }
// add_action('pre_get_posts', function ($query) {
//     if ($query->is_search) {
//         $query->set('posts_per_page', 2);
//     }
// });


//query_post_by_custompost
function query_post_by_custompost($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'showposts'=> $numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_custompost_paged($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'showposts'=> $numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


//query_post_by_category
function query_post_by_category($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat' => $cat_id,
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_category_paged($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat' => $cat_id,
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


//query_post_by_taxonomy
function query_post_by_taxonomy($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'tax_query' => array(
                                            array(
                                                    'taxonomy' => $taxonomy_name,
                                                    'field' => 'id',
                                                    'terms' => $term_id,
                                                    'operator'=> 'IN'
                                             )),
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date'
                 ) );
    return $qr;
}
function query_post_by_taxonomy_paged($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type' => $posttype_name,
                        'tax_query' => array(
                                            array(
                                                    'taxonomy' => $taxonomy_name,
                                                    'field' => 'id',
                                                    'terms' => $term_id,
                                                    'operator'=> 'IN'
                                             )),
                        'showposts'=>$numPost,
                        'order' => 'DESC',
                        'orderby' => 'date',
                        'paged'=> (get_query_var('paged')) ? get_query_var('paged') : 1
                 ) );
    return $qr;
}


//query_page_by_page_parent
function query_page_by_page_parent($page_id){
    $qr =  new WP_Query( array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $page_id,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                 ) );
    return $qr;
}


//query_search_post
function query_search_post($keyword, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'post_type'     =>  'post',
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'date',
                 ) );
    return $qr;
}


// Remove Contact Form 7 script and css, add call funtion in contact.php
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

//call funtion in contact.php
// if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
// if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }


//Edit Footer on Admin
if (!function_exists('remove_footer_admin')) {
    function remove_footer_admin () {
        echo 'Thiết kế website bởi <a href="" target="_blank">GCO</a>';
    }
    add_filter('admin_footer_text', 'remove_footer_admin');
}


//Close all Update
if (!function_exists('remove_core_updates')) {
    function remove_core_updates() {
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');
}

?>