<div class="col-lg-3 col-md-5">
	<aside class="aside-wrap">
		<h2 class="text-uppercase medium s16 b7 text-white text-center py-4 aside-tit">
			Đặt xe
		</h2>
		<?php get_template_part("resources/views/form-book-car"); ?>
	</aside>
</div>

<style type="text/css">
	.index-frm-wrap form .row > div {
	    flex: 0 0 100%;
	    max-width: 100%;
	}
	.index-frm-wrap form .btn {
		width: 100%;
	}
</style>
