<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in' => $category_ids,
	    'post__not_in' => array($post->ID),
	    'posts_per_page'=> 6,
	    'ignore_sticky_posts'=>1
    );
    $query = new wp_query( $args );
?>

<div class="bdetail-re">
    <h2 class="s18 text-uppercase bdetail-re-tit">Bài viết liên quan</h2>

    <div class="owl-carousel bdetail-re-slider">

        <?php
            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

            $post_id = get_the_ID();
            $post_title = get_the_title($post_id);
            $post_content = get_the_content($post_id);
            $post_date = customDate($post_id);
            $post_link = get_post_permalink($post_id);
            $post_image = getPostImage($post_id,"p-service");
            $post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
            $post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
        ?>

            <article class="bg-white bslider-item">
                <figure class="text-center bslider-img">
                    <a href="<?php echo $post_title; ?>" title="<?php echo $post_title; ?>">
                        <img src="<?php echo $post_image; ?>" title="<?php echo $post_title; ?>" alt="<?php echo $post_title; ?>">
                    </a>
                </figure>
                <figcaption class="bslider-content">
                    <h3 class="s16 light bslider-content-tit">
                        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                            <?php echo $post_title; ?>
                        </a>
                    </h3>
                </figcaption>
            </article>

        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

    </div>
</div>

