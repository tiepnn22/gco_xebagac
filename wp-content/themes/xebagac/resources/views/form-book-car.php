<?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');//Ho_Chi_Minh là mặc định ở việt nam rồi
    $date_send = date('d/m/Y H:i');
?>


<div id="regis-frm" class="b2 index-frm-wrap">
	<?php echo do_shortcode( get_field('form_book_car', 'option') ); ?>
</div>


<script type="text/javascript">
    jQuery('.wpcf7-submit').click(function(){
        jQuery('.input__date').val( '<?php echo $date_send; ?>' );
        console.log( '<?php echo $date_send; ?>' );
    });
</script>

<style type="text/css">
    .input__date { display: none; }
</style>

<?php
    // Call jss and css contact form 7
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
?>