<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="d-flex align-items-center search-frm">
    <input type="text" class="form-control" required="required" placeholder="Tìm kiếm" name="s" value="<?php echo get_search_query(); ?>">
    <button type="submit" class="btn search-btn"><i class="fas fa-search"></i></button>
</form>