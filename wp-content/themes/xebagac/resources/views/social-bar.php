<div class="bdetail-share">
    <h2 class="pb-3">Chia sẻ</h2>
    <ul class="list-unstyled share-list">
        <li>
            <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button" data-size="small">
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fxebagac.local%2Fdich-vu%2Ftaxi-tu-lai-dau-tien-tren-the-gioi-ra-mat-tai-nhat.html&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                </a>
            </div>
            <i class="fab fa-facebook-f"></i>
        </li>
        <li>
            <a href="http://twitter.com/share" class="twitter-share-button"
                data-url="<?php the_permalink(); ?>"
                data-via="wpbeginner"
                data-text="<?php the_title(); ?>"
                data-related="syedbalkhi:Founder of WPBeginner"
                data-count="vertical">
            </a>
            <i class="fab fa-twitter"></i>
        </li>
        <li>
            <a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php the_permalink(); ?>" target="_blank">
                <i class="fab fa-linkedin-in"></i>
            </a>
        </li>
    </ul>
</div>

<!--facebook-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v10.0" nonce="tptmow5z"></script>

<!--twitter-->
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>

<style type="text/css">
    .bdetail-share .list-unstyled {
        display: flex;
        display: -webkit-flex;
        align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
    }
    .bdetail-share .list-unstyled li {
        display: flex;
        align-items: center;
    -moz-align-items: center;
    -webkit-align-items: center;
    -o-align-items: center;
    -ms-align-items: center;
        position: relative;
        z-index: 1;
    }
    .bdetail-share .list-unstyled li iframe, .bdetail-share .list-unstyled li span {
        width: 20px !important;
    }
    .bdetail-share .list-unstyled li .fb-share-button, .bdetail-share .list-unstyled li .twitter-share-button {
        opacity: 0;
    }
    .bdetail-share .list-unstyled li svg {
        position: absolute;
        left: 8px;
        top: 4px;
        z-index: -1;
    }
   .bdetail-share .list-unstyled li:nth-child(3) svg {
        position: relative;
        left: auto;
        top: auto;
        z-index: 1;
        margin-left: 12px;
    }
</style>
