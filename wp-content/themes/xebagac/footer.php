<?php
	$f_logo = get_field('f_logo', 'option');
	$f_logo_name = get_field('f_logo_name', 'option');

	$f_mxh_icon_1 = get_field('f_mxh_icon_1', 'option');
	$f_mxh_icon_2 = get_field('f_mxh_icon_2', 'option');
	$f_mxh_icon_3 = get_field('f_mxh_icon_3', 'option');
	$f_mxh_link_1 = get_field('f_mxh_link_1', 'option');
	$f_mxh_link_2 = get_field('f_mxh_link_2', 'option');
	$f_mxh_link_3 = get_field('f_mxh_link_3', 'option');

	$f_address = get_field('f_address', 'option');
	$f_address_phone = get_field('f_address_phone', 'option');

	$f_contact_phone = get_field('f_contact_phone', 'option');
	$f_contact_gmail = get_field('f_contact_gmail', 'option');
	$f_contact_address = get_field('f_contact_address', 'option');

	$f_reserved = get_field('f_reserved', 'option');

	$f_message = get_field('f_message', 'option');
?>

<footer class="t4 ft">
	<div class="b5 ft-1">
		<div class="container">
			<div class="row">
				<div class="col-12 col-xs-6 col-lg-3 col-sm-6 text-lg-left text-center">
					<a href="index.html" title="" class="logoft">
						<img src="<?php echo $f_logo; ?>" alt="" title="">
						<span><?php echo $f_logo_name; ?></span>
					</a>

					<h2 class="bold s16 text-uppercase text-white mt-4 text-left title__mx">
						Mạng xã hội
					</h2>
					<ul class="list-unstyled ft-menu">
						<li>
							<a href="<?php echo $f_mxh_link_1; ?>">
								<?php echo $f_mxh_icon_1; ?>
							</a>
						</li>
						<li>
							<a href="<?php echo $f_mxh_link_2; ?>">
								<?php echo $f_mxh_icon_2; ?>
							</a>
						</li>
						<li>
							<a href="<?php echo $f_mxh_link_3; ?>">
								<?php echo $f_mxh_icon_3; ?>
							</a>
						</li>
					</ul>
				</div>

				<div class="col-12 col-xs-6 col-lg-3 col-sm-6">
					<h2 class="bold s16 text-uppercase text-white title__mx">Địa chỉ</h2>
					<ul class="list-unstyled">
						<li class="">
							<?php echo $f_address; ?>
						</li>
						<li class="d-flex">
							Tel: 
							<a href="tel:<?php echo str_replace(' ','',$f_address_phone);?>" title="">
								<?php echo $f_address_phone; ?>
							</a>
						</li>
					</ul>
				</div>

				<div class="col-12 col-xs-6 col-lg-2 col-sm-6">
					<h2 class="bold s16 text-uppercase text-white ft-tit">Thông tin</h2>

			        <?php
			            if(function_exists('wp_nav_menu')){
			                $args = array(
			                    'theme_location' => 'primary',
			                    'container_class'=>'menu-menu-chinh-container',
			                    'menu_class'=>'list-unstyled'
			                );
			                wp_nav_menu( $args );
			            }
			        ?>
			        
				</div>
				<div class="col-12 col-xs-6 col-lg-4 col-sm-6">
					<h2 class="bold s16 text-uppercase text-white ft-tit">
						Liên hệ
					</h2>
					<ul class="list-unstyled ft-add">
						<li class="d-flex">
							<i class="fas fa-phone"></i>
							<span>Điện thoại: 
								<a href="tel:<?php echo str_replace(' ','', $f_contact_phone );?>">
									<?php echo $f_contact_phone; ?>
								</a>
							</span>
						</li>
						<li class="d-flex">
							<i class="far fa-envelope"></i>
							<a href="mailto:<?php echo $f_contact_gmail; ?>">
								<?php echo $f_contact_gmail; ?>
							</a>
						</li>
						<li class="d-flex">
							<i class="fas fa-map-marker-alt"></i>
							<span><?php echo $f_contact_address; ?></span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="b6 s12 ft-last">
		<div class="container">
			<div class="text-lg-left text-center">
				<h2>
					<?php echo $f_reserved; ?>
				</h2>
			</div>
		</div>
	</div>
</footer>


<button class="backTop">
	<i class="fas fa-angle-up"></i>
</button>
<a href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>" class="phone__shaking">
	<div class="phone__icon">
		<img src="<?php echo asset('images/phone.png'); ?>" alt="phone.png">
	</div>
	<span class="text__phone">
		<?php echo get_field('h_phone', 'option'); ?>
	</span>
</a>


<?php echo $f_message; ?>


<?php wp_footer(); ?>
</body>
</html>
