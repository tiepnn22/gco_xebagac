<?php
include_once get_template_directory(). '/load/Custom_Functions.php';


// Theme option khi dùng Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Theme options', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Theme options', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
}


//Title Head Page
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_404()) {
            return __( '404 page not found', 'text_domain' );
        }

        return get_the_title();
    }
}


//Url File theme
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


//Url image theme
if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image-wc.png') : $img[0];
    }
}


//Cut String text
if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// customDate
function customDate($post_id)
{
    $monthName = get_the_date('m', $post_id);
    $date = get_the_date('d, Y', $post_id);

    $out = '';
    switch ($monthName) {
        case 1:
            $out = 'January';
            break;
        case 2:
            $out = 'February';
            break;
        case 3:
            $out = 'March';
            break;
        case 4:
            $out = 'April';
            break;
        case 5:
            $out = 'May';
            break;
        case 6:
            $out = 'June';
            break;
        case 7:
            $out = 'July';
            break;
        case 8:
            $out = 'August';
            break;
        case 9:
            $out = 'September';
            break;
        case 10:
            $out = 'October';
            break;
        case 11:
            $out = 'November';
            break;
        case 12:
            $out = 'December';
            break;
        default:
            break;
    }

    return $datetime = $out .' '. $date;
}


// Ajax Readmore_post Category
add_action('wp_ajax_Readmore_post', 'Readmore_post');
add_action('wp_ajax_nopriv_Readmore_post', 'Readmore_post');
function Readmore_post() {

    $data_offset = $_POST['data_offset'];
    $data_catid = $_POST['data_catid'];

    $data = array();

    $query =  new WP_Query( array(
            'cat'       => $data_catid,
            'offset'    => $data_offset,
            'showposts' => 7,
            'order'     => 'DESC',
            'orderby'   => 'date'
    ) );
    
    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

    $post_id = get_the_ID();
    $post_title = get_the_title($post_id);
    $post_content = get_the_content($post_id);
    $post_date = customDate($post_id);
    $post_link = get_post_permalink($post_id);
    $post_image = getPostImage($post_id,"p-service");
    $post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
    
    $data['result'] .= '
        <div class="sblog-item">
            <div class="row">
                <div class="col-lg-5">
                    <figure class="text-center sblog-img">
                        <a href="'.$post_link.'" title="'.$post_title.'">
                            <img src="'.$post_image.'" title="'.$post_title.'" alt="'.$post_title.'">
                        </a>
                    </figure>
                </div>
                <div class="col-lg-7">
                    <figcaption class="bslider-content">
                        <time datetime="2018-06-04" class="t2 s14 btime">
                            '.$post_date.'
                        </time>
                        <h3 class="s24 light bslider-content-tit">
                            <a href="'.$post_link.'" title="'.$post_title.'">
                                '.$post_title.'
                            </a>
                        </h3>

                        <div class="bslider-content-wrap">
                            <p>
                                '.$post_excerpt.'
                            </p>
                        </div>
                        <div class="text-lg-left text-center">
                            <a href="'.$post_link.'" title="'.$post_title.'" class="btn more-btn">
                                Chi tiết
                            </a>
                        </div>
                    </figcaption>
                </div>
            </div>
        </div>
    ';
    
    endwhile; wp_reset_postdata(); else: $data['exit'] = 2; endif;

    echo json_encode($data);
    die();
}


// Ajax Readmore_post Search
add_action('wp_ajax_Readmore_post_search', 'Readmore_post_search');
add_action('wp_ajax_nopriv_Readmore_post_search', 'Readmore_post_search');
function Readmore_post_search() {

    $data_offset_search = $_POST['data_offset_search'];
    $data_keyword = $_POST['data_keyword'];

    $data = array();

    $query =  new WP_Query( array(
            's'              => $data_keyword,
            'post_type'      => 'post',
            'offset'         => $data_offset_search,
            'showposts'      => 7,
            'order'          => 'DESC',
            'orderby'        => 'date',
    ) );
    
    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

    $post_id = get_the_ID();
    $post_title = get_the_title($post_id);
    $post_content = get_the_content($post_id);
    $post_date = customDate($post_id);
    $post_link = get_post_permalink($post_id);
    $post_image = getPostImage($post_id,"p-service");
    $post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
    
    $data['result'] .= '
        <div class="sblog-item">
            <div class="row">
                <div class="col-lg-5">
                    <figure class="text-center sblog-img">
                        <a href="'.$post_link.'" title="'.$post_title.'">
                            <img src="'.$post_image.'" title="'.$post_title.'" alt="'.$post_title.'">
                        </a>
                    </figure>
                </div>
                <div class="col-lg-7">
                    <figcaption class="bslider-content">
                        <time datetime="2018-06-04" class="t2 s14 btime">
                            '.$post_date.'
                        </time>
                        <h3 class="s24 light bslider-content-tit">
                            <a href="'.$post_link.'" title="'.$post_title.'">
                                '.$post_title.'
                            </a>
                        </h3>

                        <div class="bslider-content-wrap">
                            <p>
                                '.$post_excerpt.'
                            </p>
                        </div>
                        <div class="text-lg-left text-center">
                            <a href="'.$post_link.'" title="'.$post_title.'" class="btn more-btn">
                                Chi tiết
                            </a>
                        </div>
                    </figcaption>
                </div>
            </div>
        </div>
    ';
    
    endwhile; wp_reset_postdata(); else: $data['exit'] = 2; endif;

    echo json_encode($data);
    die();
}


